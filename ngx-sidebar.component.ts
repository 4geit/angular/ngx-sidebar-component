import { Component, OnInit } from '@angular/core';

import { NgxSidebarService } from '@4geit/ngx-sidebar-service';

@Component({
  selector: 'ngx-sidebar',
  template: require('pug-loader!./ngx-sidebar.component.pug')(),
  styleUrls: ['./ngx-sidebar.component.scss']
})
export class NgxSidebarComponent implements OnInit {

  constructor(
    private sidebarService: NgxSidebarService
  ) { }

  ngOnInit() {
  }

}
