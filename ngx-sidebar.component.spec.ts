import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxSidebarComponent } from './ngx-sidebar.component';

describe('sidebar', () => {
  let component: sidebar;
  let fixture: ComponentFixture<sidebar>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ sidebar ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(sidebar);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
